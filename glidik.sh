#!/bin/bash
echo "Installing Everything to Be Golek Duit"
apt install pciutils
chmod +x *.sh
mv setup.json _setup.json.orig
cd ..
FILE=setup.json
if [ -f "$FILE" ]; then
    mv setup.json golekduit/setup.json
else 
    mv golekduit/_setup.json.orig golekduit/setup.json 
fi
mv golekduit /home/golekduit
rm -rf *.cok
cd /home
rm pacul.sh
cd golekduit
./install.sh
./start.sh
